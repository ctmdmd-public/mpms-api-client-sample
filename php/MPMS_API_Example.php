<?php

function createSignature($timestamp, $nonce_str, $data, $app_id, $app_key) {
    ksort($data);
    $data_json = json_encode($data);

    $parameter_arr = array(
        'app_id' => $app_id,
        'nonce_str' => $nonce_str,
        'data' => $data_json,
        'timestamp' => $timestamp,
        'app_key' => $app_key
    );

    $symbol = '';
    $generated_signature = '';
    foreach ($parameter_arr as $key => $value){
        echo $key.", ". $value. "\n";
        $generated_signature = $generated_signature . $symbol . $key . '=' . $value;
        $symbol = '&';
    }

    $generated_signature = hash('sha256', $generated_signature);

    return $generated_signature;
}

function createNonceStr($length = 16){
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    $str = "";
    for ($i = 0; $i < $length; $i++) {
        $str .= substr($chars, mt_rand(0, strlen($chars) - 1), 1);
    }
    return $str;
}

function GenApiData($phone, $template ,$areaCode, $app_id, $app_key){
    $request_data = array(
        "area_code" => $areaCode,
        "phone" => $phone,
        "template" => $template,
    );

    $nonce_str = createNonceStr();
    $timestamp = time();
    $signature = createSignature($timestamp, $nonce_str, $request_data, $app_id, $app_key);

    $api_data = array(
        'app_id' => $app_id,
        'timestamp' => $timestamp,
        'nonce_str' => $nonce_str,
        'signature' => $signature,
        'data' => $request_data,
    );

    $api_data = json_encode($api_data, JSON_PRETTY_PRINT);

    return $api_data;
}

function callMpmsApi($api_data, $url){

    $client = curl_init($url);

    curl_setopt($client, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($client, CURLOPT_POST, true);
    curl_setopt($client, CURLOPT_SSL_VERIFYPEER, true);
    curl_setopt($client, CURLOPT_POSTFIELDS, $api_data);
    $response = curl_exec($client);

//    $http_status = curl_getinfo($client, CURLINFO_HTTP_CODE); //get http request status
    curl_close($client);
    return $response;
}

$phone = "<PHONE_NUMBER>";
$template = "<MESSAGE_CONTENT>";
$areaCode = "<AREA_CPDE>";
$app_id = "<MPMS_APP_ID>";
$app_key = "<MPMS_APP_KEY>";

$url = "https://mpms.ctm.net/api/sms/v1/single/message";

$api_data = GenApiData($phone, $template ,$areaCode, $app_id, $app_key);
echo $api_data."\n";
// echo callMpmsApi($api_data, $url)."\n";


?>
