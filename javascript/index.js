const signatureService = require("./services/signatureService");
const axios = require("axios");

let template = "<MESSAGE_CONTENT>";
let area_code = "<AREA_CODE>";
let phone = "<PHONE_NUMBER>";
let app_id = "<MPMS_APP_ID>";
let app_key = "<MPMS_APP_KEY>";
let url = "https://mpms.ctm.net/api/sms/v1/single/message";

console.log("template", template);
let data = {
    area_code: area_code,
    phone: phone,
    template: encodeStr(template)
};

let now = Math.floor(Date.now() / 1000);

let body = signatureService.genKeys(app_key, app_id, now, data);
console.log(body);


axios.post(url, body).then((res) => {
    console.log(res.data);
}).catch((err) => {
    console.log(err.response.data);
});


function encodeStr(s) {
    var json = JSON.stringify(s);
    return json.replace(/[\u007f-\uffff]/g,
        function (c) {
            return '\\u' + ('0000' + c.charCodeAt(0).toString(16)).slice(-4);
        }
    ).replace(/[\/]/g, "\\\/").replace(/[\"]/g, "").replace(/\\\\/g, "\\");
}