const crypto = require("crypto");

exports.test = () => {
    return createNonceStr();
}


exports.genKeys = (app_key,app_id,  now, data) => {
    data = ksort(data);

    let result = create_signature(app_key, app_id, now, createNonceStr(),
         JSON_stringify(data, false));

    let body = JSON_stringify({
        "app_id": app_id,
        "timestamp": now,
        "nonce_str": result.nonce_str,
        "signature": result.signature,
        "data": data
    }, false);
    return body;
}

function create_signature(app_key = null,app_id,  timestamp, nonce_str, data) {
    // console.log(data)
    let parameter_arr = {
        app_id: app_id,
        nonce_str: nonce_str,
        data: data,
        timestamp: timestamp,
        app_key: app_key
    }

    let symbol = '';
    let generated_signature = '';
    // foreach ($parameter_arr as $key => $value){
    Object.entries(parameter_arr).map((item) => {
        // console.log("enterie", item[0], item[1]);
        generated_signature = generated_signature + symbol + item[0] + '=' + item[1];
        symbol = '&';
        // console.log(generated_signature)
    })

    // generated_signature = hash('sha256', generated_signature);
    generated_signature = crypto.createHash('sha256')
        .update(generated_signature)
        .digest('hex');

    return { signature: generated_signature, nonce_str: nonce_str };
}

function createNonceStr(length = 16) {
    let chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    let str = "";
    for (let i = 0; i < length; i++) {
        // str += substr(chars, mt_rand(0, chars.length - 1), 1);
        let rand = Math.random() * (chars.length - 1);
        str += chars.slice(rand, rand + 1);
        // console.log(str + "  ." + i);
    }
    return str;
}

function ksort(obj) {
    var keys = Object.keys(obj).sort()
        , sortedObj = {};

    for (var i in keys) {
        sortedObj[keys[i]] = obj[keys[i]];
    }
    return sortedObj;
}

function JSON_stringify(s, emit_unicode) {
    var json = JSON.stringify(s);
    return emit_unicode ? json : json.replace(/[\u007f-\uffff]/g,
        function (c) {
            return '\\u' + ('0000' + c.charCodeAt(0).toString(16)).slice(-4);
        }
    ).replace(/\\\\/g, "\\");
}